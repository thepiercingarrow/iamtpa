==================================================================
https://keybase.io/thepiercingarrow
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://iamtpa.xyz
  * I am thepiercingarrow (https://keybase.io/thepiercingarrow) on keybase.
  * I have a public key with fingerprint FB9A 14B6 DC04 C367 0A33  073C F4FC 478E 057C 9F78

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01019c882cd5dfeeea12c29af906f2e963e36b35819a01ae0f15b3810843f6f83ad30a",
      "fingerprint": "fb9a14b6dc04c3670a33073cf4fc478e057c9f78",
      "host": "keybase.io",
      "key_id": "f4fc478e057c9f78",
      "kid": "01019c882cd5dfeeea12c29af906f2e963e36b35819a01ae0f15b3810843f6f83ad30a",
      "uid": "20f540e87c4f9374c0532c3c754ba619",
      "username": "thepiercingarrow"
    },
    "service": {
      "hostname": "iamtpa.xyz",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1535838195,
  "expire_in": 157680000,
  "prev": "90856967954a00c7d9be56d886d8563a1b7f348a9453cdee1bcfe12af91d95de",
  "seqno": 41,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.77
Comment: https://keybase.io/crypto

yMIvAnicrVJbSBVBGD5qZppdsNCsTFp7kNPJZnf2Nuch0i5GWUGJlVan2dkZXdKz
pz171JNI0BXyQcmiSBKKiiQLIaiHJG/d1Igki7LopQvRqxRFWc1KvfXYwDDMP9/3
zff9/HdnJPnSEja/fDpHnV+0K2G4rz3mq2hKGW8QDNuMC8EGYS+dPGi1SaNuaK9l
CkEBiEBERNclYiomo5RiUSISwgwBlUkUqZBC1YCKLiIMREwBExUD6iLQZchUpkNs
QoCFgMCscCV1Io4VdrksMxAWZUM1CZAJVDWAIQQaJExmRNZ0ChSNIKbpnFhlRz0G
N2fgKC2wbF7jl9CkvX/g/7Pv2KScBJgiA6prRGYIajIBCpQIJJoiG1gVkQeMUieM
ayhHu1U0YlGH8MjYcew6oTEg8Ndai1CvwV6iP0gL17gRXFAf388VIo7t2sSu5vUq
141Egx7PjUc8YB01Qn8kQoYVNrk0Z9RSJ2rZYSEociRxLU9TVHgqngQpAYHWRyyH
hiwPoWiqDvjy/qG1XBIBXVGRqiFFxgAQzUQGVVRT1/lWVIhFQ2NQ1jGSFUhMSkWD
MCpKvIWiiRSTCl6ofWFbCMoi94kruWbUqgxjN+ZQobG/d+cUX0Kab2pyojdmvrTU
2X+Hb1Vrys+C5q9JnYlZOaWB198Lh3KfL512fuF4YlFjaHHmfnclOzu6JifjjDRr
Y+7aAySzpmPDiYqf/v689EhJa9eKic+xO1lD9wJ9Zl5nG3t1Oje5dtPYlk63/2T6
xNVhyL6ZN5u1+MWmbWfyH7dvzV63ta381Dn/rYaON4M5qr/3oNrRPncC7UgJZPzo
mnqxqLy7uDql5dD74Zbkxg/VpUdjbSMfs2avvjbl1/YXi8qKs5fkdZdsjqUOVL4t
kRY8aunLuw71gnO/rkxf3/Rt2auuS1k3Dm7a3TE44H+9ngw+6fv07EJp9h5/Yc/D
+PHhy/H7Gfkj6OqDmYU9h9/dXn5s9Msse2xe2ZHfIVw6uA==
=yycz
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/thepiercingarrow

==================================================================
